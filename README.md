# gnome-membot

## What is this?

It's a bot for the GNOME membership and elections committee. It's there to make it easier to process membership requests and renewals.

## Will it work anywhere else?
I doubt it.

## License
GPL v3.

## Dependencies
pip install python-gitlab
